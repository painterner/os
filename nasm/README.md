#2018.8.11
package nasm to nasm.iso by 'dd' command, use virtual box to load to virtual disk, but can't load successfully. 

# somecommand
nasm nasm0.S   -->  nasm0 (bin) ,but can't run
nasm -f elf nasm0.S --> nasm0.o
ld nasm0.o -o nasm0 --> can run 

## myfirst
__code__
notion the 0x7c0h == 1984, the comment of 'Set up 4K stack space after this bootloader' is aimed to the code 'mov sp, 4096'. (May be after bootloader this space is reused as stack space?)
memory:
	       _________
	 4096 |         |SP
	      |         |  stack space
	 2272 |         |SS
              |         |  data space
	 1984 |         |DS
              |         |
	  512 |         |
	      |         |  bootloader
	    0 |_________|


__compile__
nasm -f bin -o myfirst.bin myfirst.S  ## it's the case run without -f bin ?
cp mikeos-4.6/disk_images/mikeos.flp test.flp
dd status=noxfer conv=notrunc if=myfirst.bin of==test.flp
qemu-system-i386 -fda test.flp
__TODO__
how to make the floppy formating file such as mikeos.flp ?

##TODOlist
simple file save, read
simple text editor
simple music video game

Timer, multi-processor,AVX etc., schedule

Nvidia-programmer
Nvidia-driver opensource load 
